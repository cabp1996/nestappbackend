import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');
  let port = process.env.PORT || 3000;
  await app.listen(Number(port));
}
bootstrap();
